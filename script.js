console.log(`Hello`);

let number = 2;
let getCube = number ** 3;

console.log(`The cube of ${number} is ${getCube}`);

let address = ["4t Benrosi Plaza Condominium", "Doña Soledad Extension", "Barangay Moonwalk", "Parañaque City"]
let [street, subdivision, barangay, city] = address

function printAddress (st, sub, brgy, cit){
	console.log(`I live at ${st}, ${sub}, ${brgy}, ${cit}.`);
}

printAddress(street, subdivision, barangay, city);

let animal = {
	name : "Lolong",
	type : "Salt Water Crocodile",
	weight : "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name, type, weight, measurement} = animal


console.log(`
	${name} is a ${type}. He weighed ${weight} with a measurement of ${measurement}.
	`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number)=> console.log(number));

let reduceNumber = numbers.reduce((a,b)=> a + b);

console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog("Joon", 2, "Pomeranian");
console.log(myDog);